const express = require('express');
const app = express();
var http = require('http').Server(app);
const port = 8080;

// https://github.com/progrape/node-localdb#readme
var db = require('node-localdb');
var jsonDB = db('user.json');

var ws = require('./node_modules/ws');

app.get('/', function(req, res) {
  console.log("Get /");
  // res.send('Hello World!');
  res.sendFile(__dirname + '/index.html');
});

app.listen(port, () =>
  console.log('Example app listening on port ' + port)
)

function resetDB() {
  jsonDB.find({}).then(function(oldJSON){
    newJSON = {button: '0', idle: 'true', knobValue: 50, proximityState: 'true', radioStation: '0'};
    if (oldJSON.length== 0) {
      jsonDB.insert(newJSON).then(function(u){});
    } else {
      jsonDB.remove({button: oldJSON[0]['button']}).then(function(u){
        jsonDB.insert(newJSON).then(function(u){});
      });
    }
  });
}

function updateDB(button, idle, knobValue, proximityState, radioStation) {
  jsonDB.find({}).then(function(oldJSON){
    // For reference, used in the remove funtion
    oldButtonValue = oldJSON[0]['button'];
    if (button != null) {
      oldJSON[0]['button'] = String(button);
    }
    if (idle != null) {
      oldJSON[0]['idle'] = String(idle);
    }
    if (knobValue != null) {
      oldJSON[0]['knobValue'] = knobValue;
    }
    if (proximityState != null) {
      oldJSON[0]['proximityState'] = String(proximityState);
    }
    if (radioStation != null) {
      oldJSON[0]['radioStation'] = String(radioStation);
    }
    jsonDB.remove({'button': oldButtonValue}).then(function(u){
        jsonDB.insert(oldJSON[0]).then(function(u){});
    });
  });
}

function getDB() {
  jsonDB.find({}).then(function(json){
    console.log(json[0]);
  });
}

var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({port: 40510})

wss.on('connection', function (ws) {

  console.log("New client connection");

  ws.on('message', function incoming(data) {
    console.log('On message channel received: %s', data);
    wss.clients.forEach(function each(client) {
      if (client !== ws) {
        console.log('Sending message');
        client.send(data);
      }
    });
    console.log('Resending message');
  })

  function sendLoopedMessage() {
    // console.log("Looped message");
    ws.send("Looped message");
    setTimeout(sayHi,3000);
  }

  // sendLoopedMessage();

})
