var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = 5000;

// https://github.com/progrape/node-localdb#readme
var db = require('node-localdb');
var jsonDB = db('user.json');

app.get('/', function(req, res) {
  console.log("Load index.html");
  res.sendFile(__dirname + '/index.html');
});

http.listen(port,function() {
  console.log('listening on: ' + port);
});

function resetDB() {
  jsonDB.find({}).then(function(oldJSON){
    newJSON = {button: '0', idle: 'true', knobValue: 50, proximityState: 'true', radioStation: '0'};
    if (oldJSON.length== 0) {
      jsonDB.insert(newJSON).then(function(u){});
    } else {
      jsonDB.remove({button: oldJSON[0]['button']}).then(function(u){
        jsonDB.insert(newJSON).then(function(u){});
      });
    }
  });
}

function updateDB(button, idle, knobValue, proximityState, radioStation) {
  jsonDB.find({}).then(function(oldJSON){
    // For reference, used in the remove funtion
    oldButtonValue = oldJSON[0]['button'];
    if (button != null) {
      oldJSON[0]['button'] = String(button);
    }
    if (idle != null) {
      oldJSON[0]['idle'] = String(idle);
    }
    if (knobValue != null) {
      oldJSON[0]['knobValue'] = knobValue;
    }
    if (proximityState != null) {
      oldJSON[0]['proximityState'] = String(proximityState);
    }
    if (radioStation != null) {
      oldJSON[0]['radioStation'] = String(radioStation);
    }
    jsonDB.remove({'button': oldButtonValue}).then(function(u){
        jsonDB.insert(oldJSON[0]).then(function(u){});
    });
  });
}

function getDB() {
  jsonDB.find({}).then(function(json){
    console.log(json[0]);
  });
}

io.sockets.on('connection', function(socket){

  console.log('A new client connected');

  socket.on('buttonPi', function(data){
    console.log('button: ' + data);
    socket.broadcast.emit('buttonFramer', data);
    console.log('Emitting button: ' + data);
  });
  socket.on('knobValuePi', function(data){
    console.log('knobValue: ' + data);
    socket.broadcast.emit('knobValueFramer', data);
    console.log('Emitting knobValue: ' + data);
  });
  socket.on('proximityStatePi', function(data){
    console.log('proximityState: ' + data);
    socket.broadcast.emit('proximityStateFramer', data);
    console.log('Emitting proximityState: ' + data);
  });
  socket.on('idleFramer', function(data){
    console.log('idle: ' + data);
    socket.broadcast.emit('idlePi', data);
    console.log('Emitting idlePi: ' + data);
  });
  socket.on('radioStationFramer', function(data){
    console.log('radioStation: ' + data);
    socket.broadcast.emit('radioStationPi', data);
    console.log('Emitting radioStationPi: ' + data);
  });
  socket.on('indexSend', function(data){
    console.log('indexSend: ' + data);
    socket.emit('indexReceived', data);
    console.log('Emitting indexReceived: ' + data);
  });

});


// resetDB();
// updateDB(null, null, null, null, null);
// updateDB("5", "false", 37, "true", "3");
// getDB();
